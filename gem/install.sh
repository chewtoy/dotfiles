#!/bin/sh

e='\033'
RESET="${e}[0m"
BOLD="${e}[1m"
CYAN="${e}[0;96m"
RED="${e}[0;91m"
YELLOW="${e}[0;93m"
GREEN="${e}[0;92m"

_exists() {
  command -v $1 > /dev/null 2>&1
}

# Success reporter
info() {
  echo "${CYAN}${*}${RESET}"
}

# Warn reporter
warn() {
  echo "${YELLOW}${*}${RESET}"
}

# Error reporter
error() {
  echo "${RED}${*}${RESET}"
}

# Success reporter
success() {
  echo "${GREEN}${*}${RESET}"
}

if ! [ -d ~/.config/colorls/ ]; then
  sudo -v
  sudo gem install colorls --silent

  mkdir -p ~/.config/colorls/
  success "Created ~/.config/colorls/..."

  cp -u $DOTFILES/gem/dark_colors.yaml ~/.config/colorls/
  success "Moved $DOTFILES/gem/dark_colors.yaml to ~/.config/colorls/..."
else
  cp -u $DOTFILES/gem/dark_colors.yaml ~/.config/colorls/

  sudo -v
  sudo gem update colorls

  success "Updating colorls..."
fi
