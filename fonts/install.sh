#!/bin/sh

e='\033'
RESET="${e}[0m"
BOLD="${e}[1m"
CYAN="${e}[0;96m"
RED="${e}[0;91m"
YELLOW="${e}[0;93m"
GREEN="${e}[0;92m"

_exists() {
  command -v $1 > /dev/null 2>&1
}

# Success reporter
info() {
  echo "${CYAN}${*}${RESET}"
}

# Warn reporter
warn() {
  echo "${YELLOW}${*}${RESET}"
}

# Error reporter
error() {
  echo "${RED}${*}${RESET}"
}

# Success reporter
success() {
  echo "${GREEN}${*}${RESET}"
}

URL="https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/FiraCode.zip"

install() {
	curl -L -s -o /tmp/fura.zip -z /tmp/fura.zip $URL
	unzip -q -u /tmp/fura.zip -d /tmp/FiraCode
	cp -u /tmp/FiraCode/* $1
  rm -f /tmp/fura.zip
  rm -rf /tmp/FiraCode
}

mkdir -p ~/.local/share/fonts
success "Updating .fonts..."
install ~/.local/share/fonts
