```sh
sudo -- sh -c 'apt update; apt upgrade -y; apt full-upgrade -y; apt autoremove -y; apt autoclean -y;'

sudo apt -y install build-essential python3-pip python3-venv libgl1 nvidia-cudnn
curl -sfL git.io/antibody | sudo sh -s - -b /usr/local/bin
gem update --system
gem update --system 3.0.6
gem -v
sudo locale-gen "en_US.UTF-8"
```

![.dotfiles Screenshot](screenshot.png)

## Installation

Dotfiles are installed by running one of the following commands in your terminal, just copy one of the following commands and execute in the terminal:

**via `curl`**

```sh
bash -c "$(curl -fsSL https://gitlab.com/chewtoy/dotfiles/raw/master/installer)"
```

**via `wget`**

```sh
bash -c "$(wget https://gitlab.com/chewtoy/dotfiles/raw/master/installer -O -)"
```

Tell Git who you are using these commands:

```console
git config -f ~/.gitlocal user.email "email@yoursite.com"
git config -f ~/.gitlocal user.name "Name Lastname"
```

## Updating

Use single command to get latest updates:

```console
update
```

## SCII Generator
```console
http://patorjk.com/software/taag/#p=display&h=1&v=0&f=Slant&t=.dotFiles
https://github.com/caarlos0/dotfiles
```


## Installation

### Dependencies

First, make sure you have all those things installed:

- `git`: to clone the repo
- `curl`: to download some stuff
- `tar`: to extract downloaded stuff
- `zsh`: to actually run the dotfiles
- `sudo`: some configs may need that

### Install

Then, run these steps:

```console
$ git clone https://gitlab.com/chewtoy/dotfiles.git ~/.dotfiles
$ cd ~/.dotfiles
$ ./installer
$ zsh # or just close and open your terminal again.
```

> All changed files will be backed up with a `.backup` suffix.
