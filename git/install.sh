#!/bin/sh

e='\033'
RESET="${e}[0m"
BOLD="${e}[1m"
CYAN="${e}[0;96m"
RED="${e}[0;91m"
YELLOW="${e}[0;93m"
GREEN="${e}[0;92m"

_exists() {
  command -v $1 > /dev/null 2>&1
}

# Success reporter
info() {
  echo "${CYAN}${*}${RESET}"
}

# Warn reporter
warn() {
  echo "${YELLOW}${*}${RESET}"
}

# Error reporter
error() {
  echo "${RED}${*}${RESET}"
}

# Success reporter
success() {
  echo "${GREEN}${*}${RESET}"
}

# Don't ask ssh password all the time
success "Updating git credential helper..."
git config --global credential.helper cache

# better diffs
if _exists diff-so-fancy; then
	success "Updating git core pager..."
	git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
fi

# use vscode as mergetool
if _exists code; then
	success "Updating git merge tool..."
	git config --global merge.tool vscode
	git config --global mergetool.vscode.cmd "code --wait $MERGED"
fi
