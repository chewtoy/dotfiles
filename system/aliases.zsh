#!/bin/sh

_exists() {
  command -v $1 > /dev/null 2>&1
}

# enable color support
if [ -x /usr/bin/dircolors ]; then
    # test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
		alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

		alias grep='grep --color=auto -i'
		alias fgrep='fgrep --color=auto -i'
    alias egrep='egrep --color=auto -i'
fi

# LSCOLORS
# Online editor: https://geoff.greer.fm/lscolors/
export LSCOLORS="Gxdxdxdxcxcxcxcxcxgaga"
export LS_COLORS='di=1;36:ln=33:so=33:pi=33:ex=32:bd=32:cd=32:su=32:sg=32:tw=36;40:ow=36;40'
# Zsh to use the same colors as ls
# Link: http://superuser.com/a/707567

# colored GCC warnings and errors
# export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# without breaking ls_style for all systems.
newline='
'
fmt1='%Y-%m-%d %H:%M'
fmt2='%Y-%m-%d %H:%M'
ls_style="--si --sort=version --time-style=+'${fmt1}${newline}${fmt2}'"

# Enable aliases to be sudo’ed
#   http://askubuntu.com/questions/22037/aliases-not-available-when-using-sudo
alias sudo='sudo '

## List installed packages
alias list="(zcat $(ls -tr /var/log/apt/history.log*); cat /var/log/apt/history.log) 2>/dev/null | zgrep -hE '^(Start-Date:|Commandline:)' | egrep -v 'aptdaemon|upgrade' | egrep '^Commandline:'"

## Custom Aliases
alias purge="sudo -- sh -c 'apt update; apt upgrade -y; apt full-upgrade -y; apt autoremove -y; apt autoclean -y;'"
alias dev='cd /mnt/e/dev'

## PG 
alias psqll='sudo pg_lsclusters'
alias psqld='psqls stop; sudo pg_dropcluster 11 dev; sudo pg_createcluster 11 dev -d ~/pgdb/db -l ~/pgdb/log/log-11-dev.log; psqls start; psqll'
alias psqls='sudo pg_ctlcluster 11 dev'
alias psqlc="sudo -u postgres createuser -s $USER; sudo -u postgres createdb $USER; psql -c \"ALTER USER $USER PASSWORD 'root';\";"
alias psqlp='psql -p'

# Get Ubuntu Updates, and update npm and its installed packages
alias update="$DOTFILES/script/update"

# View files/folder alias using colorsls (https://github.com/athityakumar/colorls)
alias l='colorls --group-directories-first --almost-all'
alias ll='colorls --group-directories-first --almost-all --long'

## General Aliases
alias listf="ls -p --time=creation | grep -v / | awk -F'.' '{print \$1}'"   # list files in dir without ext
alias cp='cp -iv'                                                           # Preferred 'cp' implementation
alias mv='mv -iv'                                                           # Preferred 'mv' implementation
alias mkdir='mkdir -pv'                                                     # Preferred 'mkdir' implementation
alias less='less -FSRXc'                                                    # Preferred 'less' implementation
alias ~='cd ~'                                                              # ~:            Go Home
alias c='clear'                                                             # c:            Clear terminal display
alias clr='clear'																														# Just bcoz clr shorter than clear
alias q="~ && clear"																												# Go to the /home/$USER (~) directory and clears window of your terminal
alias f='open -a Finder ./'                                                 # f:            Opens current directory in MacOS Finder
alias rmrf="rm -rf"
alias psef="ps -ef"
alias mkdir="mkdir -p"
alias cp="cp -r"
alias scp="scp -r"
alias lsa='ls -aF'
alias lsl='ls -lF'
alias lsf='ls -F'
alias lls='ls -FGlAhp'                                                      # Preferred 'ls' implementation
alias lltr='ls -altr'
alias lsd="ls -lFd ${ls_style}"
alias la="ls -aF ${ls_style}"
alias lA="ls -AF ${ls_style}"
# alias ll="ls -alF ${ls_style}"
alias lL="ls -alFi ${ls_style}"
alias lf="ls -F ${ls_style}"
alias lg="ls -gF ${ls_style}"
alias lS="ls -lSF ${ls_style}"
alias llt="ll -tr ${ls_style}"
chpwd() { ll; }                                                             # Auto list directory contents upon enter
# cd() { builtin cd "$@"; ll; }                                               # Always list directory contents upon 'cd'
alias cd..='cd ../'                                                         # Go back 1 directory level (for fast typers)
alias ..='cd ../'                                                           # Go back 1 directory level
alias ...='cd ../../'                                                       # Go back 2 directory levels
alias .3='cd ../../../'                                                     # Go back 3 directory levels
alias .4='cd ../../../../'                                                  # Go back 4 directory levels
alias .5='cd ../../../../../'                                               # Go back 5 directory levels
alias .6='cd ../../../../../../'                                            # Go back 6 directory levels
alias which='type -all'                                                     # which:        Find executables
alias path="echo -e ${PATH//:/\\n}"                                         # path:         Echo all executable Paths
alias show_options='shopt'                                                  # Show_options: display bash options settings
alias fix_stty='stty sane'                                                  # fix_stty:     Restore terminal settings when screwed up
alias cic='set completion-ignore-case On'                                   # cic:          Make tab-completion case-insensitive
mcd () { mkdir -p "$1" && cd "$1"; }                                        # mcd:          Makes new Dir and jumps inside
trash () { command mv "$@" ~/.Trash ; }                                     # trash:        Moves a file to the MacOS trash
ql () { qlmanage -p "$*" >& /dev/null; }                                    # ql:           Opens any file in MacOS Quicklook Preview

explorer () {
    if [ -z "$1" ]; then
        explorer.exe .
    else
        explorer.exe "$(wslpath -w "$1")"
    fi
}

command -v vim &> /dev/null && alias vi='vim'; \
  alias svi='sudo vim'; \
  alias svim='svi' || :
alias jobs='jobs -l'
alias pt='ping -c 3'
alias ptg='pt 8.8.8.8'
alias sshy='ssh -Y'

alias zz='z -c'      # restrict matches to subdirs of $PWD
alias zi='z -i'      # cd with interactive selection
alias zf='z -I'      # use fzf to select in multiple matches
alias zb='z -b'      # quickly cd to the parent directory

alias duf="du -sh * | sort -hr"
alias croot='cd "$(git rev-parse --show-toplevel)"'

# quick hack to make watch work with aliases
alias watch='watch '

# Commands Shortcuts
alias e="$EDITOR"

alias -- +x='chmod +x'
alias x+='chmod +x'
alias rwx='chmod a=rwx,u+t'

# Open aliases
alias open='open_command'
alias o='open'
alias oo='open .'

# Quick jump to dotfiles
alias dotfiles="cd $DOTFILES"

# Show $PATH in readable view
alias path='echo -e ${PATH//:/\\n}'

# Download web page with all assets
alias getpage='wget --no-clobber --page-requisites --html-extension --convert-links --no-host-directories'

# Download file with original filename
alias get="curl -O -L"

# Add an "alert" alias for long running commands.
# Use like so:
#   sleep 10; alert
if _exists notify-send; then
  alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
fi

# My IP
alias myip='ifconfig | sed -En "s/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p"'

# Password generator
# Gemnerate random password, copies it into clipboard and outputs it to terminal
if _exists pbcopy; then
  alias password='openssl rand -base64 ${1:-9} | pbcopy ; echo "$(pbpaste)"'
elif _exists xcopy; then
  alias password='openssl rand -base64 ${1:-9} | xcopy ; echo "$(xpaste)"'
else
  alias password='openssl rand -base64 ${1:-9}; echo "$(xpaste)"'
fi

# open, pbcopy and pbpaste on linux
if [ "$(uname -s)" != "Darwin" ]; then
	if [ -z "$(command -v pbcopy)" ]; then
		if [ -n "$(command -v xclip)" ]; then
			alias pbcopy="xclip -selection clipboard"
			alias pbpaste="xclip -selection clipboard -o"
		elif [ -n "$(command -v xsel)" ]; then
			alias pbcopy="xsel --clipboard --input"
			alias pbpaste="xsel --clipboard --output"
		fi
	fi
	if [ -e /usr/bin/xdg-open ]; then
		alias open="xdg-open"
	fi
fi

# Folders Shortcuts
# [ -d ~/Dropbox ]              && alias dr='cd ~/Dropbox'
# [ -d ~/Downloads ]            && alias dl='cd ~/Downloads'
# [ -d ~/Desktop ]              && alias dt='cd ~/Desktop'
# [ -d ~/Projects ]             && alias pj='cd ~/Projects'
# [ -d ~/Projects/Forks ]       && alias pjf='cd ~/Projects/Forks'
# [ -d ~/Projects/Job ]         && alias pjj='cd ~/Projects/Job'
# [ -d ~/Projects/Playground ]  && alias pjl='cd ~/Projects/Playground'
# [ -d ~/Projects/Repos ]       && alias pjr='cd ~/Projects/Repos'

# Use tldr as help util
if _exists tldr; then
  alias help="tldr"
fi
