#!/bin/zsh
# Enable the addition of zsh hook functions
autoload -Uz add-zsh-hook

# Set the tab title to the current working directory before each prompt
function tabTitle () {
  window_title="\033]0;${PWD##*/}\007"
  echo -ne "$window_title"
}

# Executes tabTitle before each prompt
add-zsh-hook precmd tabTitle
