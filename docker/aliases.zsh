#!/bin/sh
docker_prune() {
	docker system prune --volumes -fa
}

alias docker="sudo docker"
alias docker-compose="sudo docker-compose"
