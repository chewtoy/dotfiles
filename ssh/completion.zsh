#!/bin/sh

# Loading ssh key based on host
# .zshrc on multiple hosts I need to have the option to only load certain keys based on the host 
# Say, id_rsa_private_1 is only for work hosts. When I log into a non-work host I only want id_rsa_private_2 to be loaded
# if [[ "$HOST" =~ blhablah ]] then
#   zstyle :omz:plugins:ssh-agent identities id_rsa_private_2
# else
#   zstyle :omz:plugins:ssh-agent identities id_rsa_private_1 id_rsa_private_2
# fi    
