#!/bin/sh

e='\033'
RESET="${e}[0m"
BOLD="${e}[1m"
CYAN="${e}[0;96m"
RED="${e}[0;91m"
YELLOW="${e}[0;93m"
GREEN="${e}[0;92m"

_exists() {
  command -v $1 > /dev/null 2>&1
}

# Success reporter
info() {
  echo "${CYAN}${*}${RESET}"
}

# Warn reporter
warn() {
  echo "${YELLOW}${*}${RESET}"
}

# Error reporter
error() {
  echo "${RED}${*}${RESET}"
}

# Success reporter
success() {
  echo "${GREEN}${*}${RESET}"
}

if ! [ -d ~/.ssh/ ]; then
  mkdir -p ~/.ssh/
  success "Created ~/.ssh..."
fi

test -L ~/.ssh/config || {
  success "Updating .ssh/config to .ssh/config.local..."
  mv -f ~/.ssh/config ~/.ssh/config.local
  success "Updating .ssh/config to $DOTFILES/ssh/config..."
  ln -s $DOTFILES/ssh/config ~/.ssh/config
}
test -f ~/.ssh/config.local || touch ~/.ssh/config.local
