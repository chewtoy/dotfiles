#!/bin/zsh

# display how long all tasks over 10 seconds take
export REPORTTIME=10
