#!/bin/sh

e='\033'
RESET="${e}[0m"
BOLD="${e}[1m"
CYAN="${e}[0;96m"
RED="${e}[0;91m"
YELLOW="${e}[0;93m"
GREEN="${e}[0;92m"

_exists() {
  command -v $1 > /dev/null 2>&1
}

# Success reporter
info() {
  echo "${CYAN}${*}${RESET}"
}

# Warn reporter
warn() {
  echo "${YELLOW}${*}${RESET}"
}

# Error reporter
error() {
  echo "${RED}${*}${RESET}"
}

# Success reporter
success() {
  echo "${GREEN}${*}${RESET}"
}

REPO="$DOTFILES/n/n/.repo/bin/n"
BIN="$DOTFILES/n/bin"

if ! [ -d $BIN/ ]; then
  mkdir -p $BIN/
  success "Created $BIN/..."

  cp -u $REPO $BIN/
  success "Moved $REPO to $BIN/n..."
else
  cp -u $REPO $BIN/
  success "Updating n (node version management)..."
fi
