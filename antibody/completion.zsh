#!/bin/zsh

# #############################################################################################################################################
# Settings for https://github.com/marzocchi/zsh-notify
# #############################################################################################################################################
# Set a custom title for error and success notifications, when using the built-in notifier.
# zstyle ':notify:*' error-title "Command failed"
# zstyle ':notify:*' success-title "Command finished"

# The string #{time_elapsed} will be replaced with the command run time.
# zstyle ':notify:*' error-title "Command failed (in #{time_elapsed} seconds)"
# zstyle ':notify:*' success-title "Command finished (in #{time_elapsed} seconds)"

# Change the notifications icons for failure or success. Any image path or URL (Mac OS only) should work.
# zstyle ':notify:*' error-icon "/path/to/error-icon.png"
# zstyle ':notify:*' success-icon "/path/to/success-icon.png"

# Set a sound for error and success notifications, when using the built-in notifier. On Linux you should specify the path to an audio file.
# zstyle ':notify:*' error-sound "Glass"
# zstyle ':notify:*' success-sound "default"

# Have the terminal come back to front when the notification is posted.
# zstyle ':notify:*' activate-terminal yes

# Disable setting the urgency hint for the terminal when the notification is posted (Linux only).
# zstyle ':notify:*' disable-urgent yes

# Set a different timeout for notifications for successful commands (notifications for failed commands are always posted).
# zstyle ':notify:*' command-complete-timeout 15

# Replace the built-in notifier with a custom one at ~/bin/my-notifier. 
# The custom notifier will receive the notification type (error or success) as the first argument, the time elapsed as the second argument, and the command line as standard input.
# zstyle ':notify:*' notifier ~/bin/my-notifier

# Disable error reporting (or send it somewhere else)
# zstyle ':notify:*' error-log /dev/null
# #############################################################################################################################################
