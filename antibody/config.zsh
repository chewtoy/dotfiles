#!/bin/zsh

# z.lua Enhanced matching
export _ZL_MATCH_MODE=1

function _z() { _zlua "$@"; }

# #############################################################################################################################################
# Settings for https://github.com/MichaelAquilina/zsh-you-should-use
# #############################################################################################################################################
# By default, you-should-use will display its reminder message before a command has exected. 
# However, you can choose to display the mesasge after a command has executed by setting the value of YSU_MESSAGE_POSITION.
export YSU_MESSAGE_POSITION="after"

# To only display best match (default): export YSU_MODE=BESTMATCH
# To display all matches: export YSU_MODE=ALL
export YSU_MODE=ALL

# Customising Messages
# $(tput setaf 1) generates the escape code terminals use for red foreground text. $(tput sgr0) sets the text back to a normal color.
# export YSU_MESSAGE_FORMAT="$(tput setaf 1)Hey! I found this %alias_type for %command: %alias$(tput sgr0)"
# #############################################################################################################################################

# #############################################################################################################################################
# Settings for https://github.com/denysdovhan/spaceship-prompt
# #############################################################################################################################################
# Theme
SPACESHIP_PROMPT_ORDER=(
  user          # Username section
  dir           # Current directory section
  host          # Hostname section
  git           # Git section (git_branch + git_status)
  hg            # Mercurial section (hg_branch  + hg_status)
  # package       # Package version
  node          # Node.js section
  ruby          # Ruby section
  elixir        # Elixir section
  xcode         # Xcode section
  swift         # Swift section
  golang        # Go section
  php           # PHP section
  rust          # Rust section
  haskell       # Haskell Stack section
  julia         # Julia section
  docker        # Docker section
  aws           # Amazon Web Services section
  venv          # virtualenv section
  conda         # conda virtualenv section
  pyenv         # Pyenv section
  dotnet        # .NET section
  ember         # Ember.js section
  #kubecontext   # Kubectl context section
  terraform     # Terraform workspace section
  # time          # Time stamps section
  # exec_time     # Execution time
  line_sep      # Line break
  battery       # Battery level and status
  vi_mode       # Vi-mode indicator
  # jobs          # Background jobs indicator
  # exit_code     # Exit code section
  char          # Prompt character
)
SPACESHIP_RPROMPT_ORDER=(
  rprompt_prefix
  # line_sep      # Line break
  exit_code     # Exit code section
  exec_time     # Execution time
  jobs          # Background jobs indicator
  time          # Time stamps section
  rprompt_suffix
)

spaceship_rprompt_prefix() {
  echo -n '%{'$'\e[1A''%}'
}
spaceship_rprompt_suffix() {
  echo -n '%{'$'\e[1B''%}'
}

SPACESHIP_CHAR_SYMBOL="❯"
SPACESHIP_CHAR_SUFFIX=" "

SPACESHIP_TIME_SHOW="true"
SPACESHIP_TIME_12HR="true"
# #############################################################################################################################################
