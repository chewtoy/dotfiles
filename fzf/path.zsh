#!/bin/sh

# Setup fzf
# ---------
if [[ ! "$PATH" == *$DOTFILES/fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}$DOTFILES/fzf/bin"
fi
